import React from 'react';
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3, 0, 2),
    textAlign: "center",
    color: "deeppink",
    fontSize: '40px',
    fontFamily: "Montserrat",
    fontWeight: 500
  }
}))

const Header = () => {
  const styles = useStyles()
  return (
    <Typography className={styles.root} variant="h5" component="h1">
      Ultimate Form
    </Typography>
  );
};

export default Header;