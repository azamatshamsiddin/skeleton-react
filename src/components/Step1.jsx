import React from 'react';
import MainContainer from "./MainContainer";
import { Typography } from "@material-ui/core";

const Step1 = () => {
  return (
    <MainContainer>
      <Typography component="h2" variant="h5">
        Step1
      </Typography>
    </MainContainer>
  );
};

export default Step1;