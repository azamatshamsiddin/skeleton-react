import React from "react"
import Header from "./components/Header";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Result from "./components/Result";
import Step1 from "./components/Step1";
import Step2 from "./components/Step2";
import Step3 from "./components/Step3";

function App() {
  
  return (
    <>
      <Header/>
      <Router>
        <Routes>
          <Route index path="/" element={<Step1/>}/>
          <Route index path="/step2" element={<Step2/>}/>
          <Route index path="/step3" element={<Step3/>}/>
          <Route index path="/result" element={<Result/>}/>
        </Routes>
      </Router>
    
    </>
  );
}


export default App;
